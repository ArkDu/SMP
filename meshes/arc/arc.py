import numpy as np
import struct

# mesh file
mesh = open("mesh.x", "wb")

# number of blocks
#mesh.write("1\n")
mesh.write(struct.pack('i',1))

# cells in direction
Nphi   = 8;
Nr     = 4;
Ntheta = 4;

# axis range
phiMin   = 0.0;
phiMax   = np.pi/3.0;
thetaMin = np.pi/4.0
thetaMax = 3.0*np.pi/8.0;
rMin     = 0.8;
rMax     = 1.0;

# size of block
# i - theta, j - phi, k - r
mesh.write(struct.pack('iii', Ntheta+1, Nphi+1, Nr+1))
#mesh.write(repr(Ntheta+1) + " " + repr(Nphi+1) + " " + repr(Nr+1) + "\n")

# discretize
phis   = np.linspace(phiMin,   phiMax,   num = Nphi+1)
thetas = np.linspace(thetaMin, thetaMax, num = Ntheta+1)
rs     = np.linspace(rMin,     rMax,     num = Nr+1)

# write coordinates
for k in range(Nr+1):
  for j in range(Nphi+1):
    for i in range(Ntheta+1):
      mesh.write(struct.pack('d', rs[k]*np.sin(thetas[i])*np.cos(phis[j])));
      #mesh.write(repr(rs[k]*np.sin(thetas[i])*np.cos(phis[j])) + " ");
#mesh.write("\n")

for k in range(Nr+1):
  for j in range(Nphi+1):
    for i in range(Ntheta+1):
      mesh.write(struct.pack('d', rs[k]*np.sin(thetas[i])*np.sin(phis[j])));
      #mesh.write(repr(rs[k]*np.sin(thetas[i])*np.sin(phis[j])) + " ");
#mesh.write("\n")

for k in range(Nr+1):
  for j in range(Nphi+1):
    for i in range(Ntheta+1):
      mesh.write(struct.pack('d', rs[k]*np.cos(thetas[i])));
      #mesh.write(repr(rs[k]*np.cos(thetas[i])) + " ")
#mesh.write("\n")

mesh.close();

# mapfile
bcMap = open("mapfile", "w")

# title line
bcMap.write("Type Block# Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 - Block# ")
bcMap.write("Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 Flip\n")

bcMap.write("FarField 1 1\n")
bcMap.write("ViscousEall 1 2\n")
bcMap.write("ViscousEall 1 3\n")
bcMap.write("FarField 1 4\n")
bcMap.write("ViscousEall 1 5\n")
bcMap.write("ViscousEall 1 6\n")

bcMap.close()
