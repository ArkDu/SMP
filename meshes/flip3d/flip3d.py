import numpy as np

# mesh file
mesh = open("mesh.x", "w")

# number of blocks
mesh.write("5\n") 

# uniform grid space step
L =  1    # length
N = 16    # grid cells per unit length

# all blocks are equivalent to a cube.
# plot3D format header: block index range
mesh.write(repr(L*N+1) + ' ' + repr(2*L*N+1)   + ' ' + repr(L*N+1) + "\n") 
mesh.write(repr(L*N+1) + ' ' + repr(2*L*N+1)   + ' ' + repr(L*N+1) + "\n") 
mesh.write(repr(L*N+1) + ' ' + repr(2*L*N+1)   + ' ' + repr(L*N+1) + "\n") 
mesh.write(repr(L*N+1) + ' ' + repr(2*L*N+1)   + ' ' + repr(L*N+1) + "\n") 
mesh.write(repr(L*N+1) + ' ' + repr(2*L*N+1)   + ' ' + repr(L*N+1) + "\n") 

mesh.close()

# mapfile
bcMap = open("mapfile", "w")

# title line
bcMap.write("Type Block# Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 - Block# ")
bcMap.write("Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 Flip\n")

# block 1
bcMap.write("Block2Block 1 1 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + " 3 4 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + "\n")
bcMap.write("EulerWall 1 2\n")
bcMap.write("Block2Block 1 3 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + " 2 6 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + "\n")
bcMap.write("Block2Block 1 4 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + " 5 1 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + "\n")
bcMap.write("EulerWall 1 5\n")
bcMap.write("Block2Block 1 6 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + " 4 3 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + "\n")

#block 2
bcMap.write("Block2Block 2 1 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + " 3 3 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + " yes\n")
bcMap.write("EulerWall 2 2\n")
bcMap.write("EulerWall 2 3\n")
bcMap.write("Block2Block 2 4 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + " 5 3 " + repr(L*N+1) + " 1 1 " + repr(2*L*N+1) + " yes\n")
bcMap.write("EulerWall 2 5\n")

#block 3
bcMap.write("EulerWall 3 1\n")
bcMap.write("EulerWall 3 2\n")
bcMap.write("EulerWall 3 5\n")
bcMap.write("Block2Block 3 6 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + " 4 1 1 " + repr(2*L*N+1) + " " + repr(L*N+1) + " 1 yes\n")

#block 4
bcMap.write("EulerWall 4 2\n")
bcMap.write("Block2Block 4 4 1 " + repr(2*L*N+1) + " 1 " + repr(L*N+1) + " 5 6 1 " + repr(L*N+1) + " 1 " + repr(2*L*N+1) + " yes\n")
bcMap.write("EulerWall 4 5\n")
bcMap.write("EulerWall 4 6\n")

#block 5
bcMap.write("EulerWall 5 2\n")
bcMap.write("EulerWall 5 4\n")
bcMap.write("EulerWall 5 5\n")

bcMap.close()
