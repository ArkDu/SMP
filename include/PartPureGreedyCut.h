#ifndef PARTPUREGREEDYCUT_H
#define PARTPUREGREEDYCUT_H

#include <cmath>
#include "Block.h"
#include "BlockGraph.h"
#include "BlockHeap.h"
#include "Partition.h"
#include "PartMethod.h"

struct NbrBlock {
  int    bid;
  double tCommSave;
};

bool operator<(const NbrBlock& lhs, const NbrBlock& kkkkkrhs);

bool operator>(const NbrBlock& lhr, const NbrBlock& rhs);


class PartPureGreedyCut : public PartMethod
{
  public:  //---------- public member functions ----------//

  int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid);

  int setup_large_blocks(BlockGraph& bg, vector<int>& bids);

  int cut_large_blocks(BlockGraph& bg, vector<int>& bids);

  int grow_small_blocks(BlockGraph& bg, vector<int>& bids);

  int sweep_assign_blocks(BlockGraph& bg, vector<int>& bids);

  int cut_assign_blocks(BlockGraph& bg, vector<int>& bids);

  private: //---------- private attributes      ----------//
  vector<bool>          _isDones;      // is block assigned
  vector< vector<int> > _partBids;     // block ids in a partition
  vector<int>           _largeBids;    // large block IDs
  vector<int>           _npLargeBlks;  // #parts assigned to each large block
};

#endif
