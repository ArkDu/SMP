#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <cmath>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include <assert.h>

// by default there is no verbose output
#ifndef verbose
#define verbose 0
#endif


/*------------------           types          ------------------*/
typedef unsigned int uint;


/*------------------         constants        ------------------*/

const double DLARGE         = 1.0E14;
const double DSMALL         = 1.0E-10;
const int    PARTITION_NULL = -1;
const int    INDEX_NULL     = -1;

const std::vector<int> VECTOR_INT_NULL;

enum BCType {BC_FARFIELD, BC_VISCOUSWALL, BC_EULERWALL, 
             BC_SYMMETRY, BC_BLOCK2BLOCK};


/*------------------         functions        ------------------*/

bool is_prime(int n);

int vector_index(std::vector<int>& intVec, int val);

#endif
