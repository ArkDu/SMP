/*------------------------------------------------------------------------------
Purpose:
  Define infrastructure for geometry/grid blocks including
  * BcMap      - map struct for each bc
  * BlockFace  
  * Block      
  * BlockCut   - the cut of a block
  Note that geometry block and grid are using the same block class

TODO:
  not sure if I need to define <,>,==,!= for using STL list with BcMap.

Author:
  Hengjie Wang, hengjiew@uci.edu
------------------------------------------------------------------------------*/

#ifndef BLOCK_H
#define BLOCK_H

#include <cstring>
#include <iomanip>
#include "common.h"
using namespace std;

const int BLOCK_NULL      = -1;
const int FACE_NULL       = -1;
const int RANGE_NULL      = -1;

const int EdgeMap3d[3][2] = {{1,2}, {2,0}, {0,1}};


struct BcMap
{
  string bcType;     // bc type per map
  int    rngs[6];    // index range per map
  int    toBlk;      // target block per map
  int    toFace;     // target face per map
  int    toRngs[6];  // index range on target face
  bool   isFlip;     // eg. x-y mapped as y-x on target face
  bool   isShMem;    // through shared memory?
  vector<int> mergedFcId;
  vector<int[6]> mergedRngs;

  bool   is_inv() const
  { return(toRngs[3] < toRngs[0] || toRngs[4] < toRngs[1] || toRngs[5] < toRngs[2]); }

  bool   is_direct(int fid) const
  { return(toRngs[3] >= toRngs[0] && toRngs[4] >= toRngs[1] && toRngs[5] >= toRngs[2] &&
           fid%3 == toFace%3 && !isFlip); }
};

typedef list<BcMap>::iterator MapIter;
typedef list<BcMap>::const_iterator CMapIter;

class BlockFace
{
  public:  //---------- public member functions ----------//
  BlockFace(int numHalo=2)
  { ID = FACE_NULL; msgLoad = 0.0; nHalo = numHalo; }

  BlockFace(const BlockFace& rhs);

  const BlockFace& operator=(const BlockFace& rhs);

  ~BlockFace();

  void set_num_halo( int numHalo )
  { nHalo = numHalo; }

  int add_map(string bcType, const int ranges[6]);
  /*----------------------------------------------------------
    Add a map info for physical boundary

    In:  bcType   -  b2b boudnary condition
         ranges   -  local index range
  ----------------------------------------------------------*/

  int add_map(string bcType, const int ranges[6],   int  toBlk, \
              int    toFace, const int toRanges[6], bool isFilp );
  /*----------------------------------------------------------
    Add a map info for block to block communication

    In:  bcType   -  b2b boudnary condition
         ranges   -  local index range
         toBlk    -  target block
         toFace   -  target face of target block
         toRanges -  idx ranges of target block
         isFilp   -  flip the map or not
  ----------------------------------------------------------*/

  int view();
  /*----------------------------------------------------------
    print the map info on current face
  ----------------------------------------------------------*/

  int map_axis(const BcMap& map, int axis);
  /*----------------------------------------------------------
    Find the mapping axis of a given axis

    In:   map    - bcmap
          axis   - input axis
    Rtn:  mapping axis for input axis

    mthd: search for the EdgeMap3d to get the target axis
          if it is flipped then choose the other axis in EdgeMap3d

    pre:  axis should be parallel to the calling face
  ----------------------------------------------------------*/

  int map_axis_inv(const BcMap& map, int toAxis);
  /*----------------------------------------------------------
    Given target axis, find the axis mapped to it
    See map_axis.

    pre:  axis should be parallel to the target face
  ----------------------------------------------------------*/

  int adjust_map_range(int side, int val, BcMap& map);
  /*----------------------------------------------------------
    Adjust the range of a map based on input and change
    its target range accordingly

    In:   side - 0-5
          val  - new range value
    Out:  map  - map to be adjusted

    pre:  * The map is on the calling face
          * side on a axis parallel with the face
  ----------------------------------------------------------*/

  int adjust_map_target_range(int side, int val, BcMap& map);
  /*----------------------------------------------------------
    Adjust the target range of a map based on input and change 
    its range accordingly

    In:   side - 0-5
          val  - new range value
    Out:  map  - map to be adjusted

    pre:  * The map is on the calling face
          * side on a axis parallel with the target face
  ----------------------------------------------------------*/

  list<BcMap>::iterator map_iter(const BcMap& map);
  /* retrun the iterator for give map, the map has to be on face */

  int is_mergable(const BcMap& map0, const BcMap& map1) const;

  public: //---------- public attributes      ----------//
  int         ID;       // face iD
  int         nHalo;
  list<BcMap> mapList;  // list of bc maps
  double      msgLoad;  // comm time, alpha-beta model
};// end BlockFace


class BlockCut
{
  public:  //---------- public member functions ----------//
  BlockCut()
  { blkID = BLOCK_NULL; axis = RANGE_NULL; pos = RANGE_NULL; \
    msgIncr = 0.0; isKept = true; }
  BlockCut(double load)
  { blkID = BLOCK_NULL; axis = RANGE_NULL; pos = RANGE_NULL; \
    msgIncr = load; isKept = true; }
  void reset()
  { blkID = BLOCK_NULL; axis = RANGE_NULL; pos = RANGE_NULL; \
    msgIncr = 0.0; isKept = true; }
  void set(int bid, int cutAxis, int cutPos, double cutMsg, bool isCutOffAppend)
  { blkID = bid; axis = cutAxis; pos = cutPos; msgIncr = cutMsg; isKept = isCutOffAppend; }
  bool operator<(const BlockCut& rhs)
  { return(msgIncr < rhs.msgIncr); }
  bool operator>(const BlockCut& rhs)
  { return(msgIncr > rhs.msgIncr); }
  bool operator<(double val)
  { return(msgIncr < val); }
  bool operator>(double val)
  { return(msgIncr > val); }
  bool operator==(const BlockCut& rhs)
  { return(   blkID == rhs.blkID && axis == rhs.axis && pos == rhs.pos 
           && msgIncr == rhs.msgIncr && isKept == rhs.isKept); }

  public:  //---------- public attributes       ----------//
  int    blkID;    // block ID
  int    axis;     // x, y, z
  int    pos;      // cut index
  double msgIncr;  // increase of msgload due to cut, can be negative
  bool   isKept;
};


class Block
{
  public:  //---------- public member functions ----------//
  // basics
  Block(int blockID = BLOCK_NULL);          // also as default constructor
  Block(const Block& rhs);                  // not copy coordinates
  const Block& operator=(const Block& rhs); // not copy coordinates
  ~Block();

  // basic setters
  void set_partition(int partitionID)
    { partID = partitionID; }
  void set_blockID(int blockID)
    { blkID = blockID; }
  void set_parentID(int prntID)
    { pBlkID = prntID; }
  void set_range(const int ranges[6])
    { memcpy(rngs, ranges, 6*sizeof(int)); }

  // basic getters
  void get_range(int ranges[6]) const
    { memcpy(ranges, rngs, 6*sizeof(int)); }
  void get_size(int blkSize[3]) const
  /* get the size of block which is the # cells in each dimension */
    { for (int i=0; i<3; ++i) blkSize[i] = rngs[i+3] - rngs[i]; }
  void get_face(int faceID, BlockFace& face)
    { face = faces[faceID]; }
  void get_partition(int& partitionID)
    { partitionID = partID; }
  int get_axes_sorted(int& dirLong, int& dirMid, int& dirShort) const;

  // basic returners
  int id() const
    { return(blkID); }
  double msg_load() const
    { return(msgLoad); }
  double latency_load() const
    { return(ltncy); }
  int work_load() const
    { return((rngs[3]-rngs[0])*(rngs[4]-rngs[1])*max(rngs[5]-rngs[2], 1)); }
  double work() const
    { return((double)(rngs[3]-rngs[0])*(rngs[4]-rngs[1])*max(rngs[5]-rngs[2], 1)); }
  int parent() const
    { return(pBlkID); }
  int partition_id() const
    { return(partID); }
  int length(int dir) const
    { return(rngs[dir+3] - rngs[dir]); }
  BlockFace* face_pt(int faceID)
    { return(&(faces[faceID])); }
  double time() const
  { return(_t); }
  double time_comp() const
  { return(_tComp); }

  int num_cell(int nHalo) const;

  int num_halo_cell(int nHalo) const;

  int num_vertex() const
  { return (rngs[5]-rngs[2]+1)*(rngs[4]-rngs[1]+1)*(rngs[3]-rngs[0]+1); }

  int area(int dir) const;

  void get_num_halo(int *nHalos);

  void get_coord_pt(int dir, double** ptCoord);

  void get_face_pt(int faceID, BlockFace** ptFc)
    { *ptFc = &(faces[faceID]); }

  int add_bc(int faceID, string boundaryType, const int ranges[6]);
  /*----------------------------------------------------------
    See add_map in BlockFace
  ----------------------------------------------------------*/

  int add_bc(int  faceID,  string boundaryType, const int ranges[6],   \
             int  toBlock, int    toFace,       const int toRanges[6], \
             bool isFlip);
  /*----------------------------------------------------------
    See add_map in BlockFace
  ----------------------------------------------------------*/

  int clear_bc(int faceID);
  /* delete all maps on a face */

  int cut_off_bc(int side, const BlockCut& cut);
  /* remove bc cut off by a given cut */

  int alloc_coord();
  /*----------------------------------------------------------
    alloc the x, y, z array

    Mthd: The actual range is the index range plus the nhalo
            on each face.

    Pre:  All the faces should already be set up.
  ----------------------------------------------------------*/

  void set_coord(int i, int j, int k, double coords[3]);
  /*----------------------------------------------------------
    Set the coordinates at given index

    In:  i,j,k    - the index
         coords[] - the coordinates

    Pre: The index must fit in the range of block. 
         Note that the block's index may not start from 0.
  ----------------------------------------------------------*/

  void get_coord(int i, int j, int k, double coords[3]);
  /*----------------------------------------------------------
    Opposite of set_coord
  ----------------------------------------------------------*/

  int copy_coord(const Block& blk);
  /*----------------------------------------------------------
    Copy the coordinates from parent block.

    In:  blk  -  parent block

    Pre: * The input blk is the parent block of present blk.
         * The index range of present blk including its halo is 
           part of the the input blk's range.
  ----------------------------------------------------------*/

  int view();

  int cmpt_msg_load(double alpha, double beta, int msgPerCell);
  /*----------------------------------------------------------
    Compute the message load of each face and whole block

    In: alpha, beta  -  alpha, beta model
        msgPerCell   -  # Bytes sent per cell
  ----------------------------------------------------------*/

  int cmpt_time(double alpha, double beta, int sizePerCell, double tPerCell);
  /*----------------------------------------------------------
    Compute time cost of a block, including comm and comp

    In: alpha, beta  -  alpha, beta model
        sizePerCell  -  # Bytes sent per cell
        tPerCell     -  comp time per cell
  ----------------------------------------------------------*/

  double time_comm(int fid, int pos, double alpha, double beta, int sizeCell);
  /*----------------------------------------------------------
    Compute communication time in given range

    In:  fid  -  starting face
         pos  -  end position
    Rtn: communication time of a chuck from rngs[fid] to pos
  ----------------------------------------------------------*/
  bool is_b2b_margin(int dir, int pos);
  /* check if a position is on b2b's margin */

  vector<Block>  mergedBlks;
  int *pBlkIdx;

  void set_alive(bool alive)
    {blkAlive = alive;}

  bool get_alive()
    {return blkAlive;}
  
  private: //---------- private attributes      ----------//
  int       blkID, pBlkID;
  int       partID;
  int       rngs[6];
  double    *x, *y, *z;
  double    msgLoad, ltncy;
  double    _t, _tComp;
  BlockFace faces[6];
  int blkAlive;

  friend class BlockGraph;

}; // end Block


#endif
