#ifndef PARTCLUSTER_H
#define PARTCLUSTER_H

#include <functional>
#include "Partition.h"
#include "PartMethod.h"


class ElementHeap
{
  public:  //---------- public member functions ----------//
  ElementHeap()
  /* default constructor */
  {};

  int top()
  /* return the block id at heap top */
  { return(_bids[0]); }

  int pop();
  /* remove the heap top */

  int pop(int bid);
  /* remove the input block */

  int heapify(int i);
  /* restore the heap order at input position */

  int find_blk(int bid);
  /* return the input blocks id's position in heap */
  
  int add(int bid, double msgIncr);
  /* Add an element and put it to right place */

  int decr(int pos, double val);
  /* decrease the weight at input position and restore the order */

  int clear();
  /* remove everything */

  int view();
  /* print all elements */

  int size()
  /* return the number of elements */
  { return((int)_bids.size()); }

  private: //---------- private attributes      ----------//
  vector<int>    _bids;     // block ids
  vector<double> _msgIncrs; // negative

  //-------------------      friendship         ----------//
  friend int _cmpr_top(ElementHeap& hp0, ElementHeap& hp1);
};


int _cmpr_top(ElementHeap& hp0, ElementHeap& hp1);
/*----------------------------------------------------------
Compare with element's top has less message load

In:  hp0, hp1 - elements heap
Rtn: -1/0/1   - hp0 has samller top, same, hp1 has
----------------------------------------------------------*/


class PartCluster : public PartMethod
{
  public:  //---------- public member functions ----------//
  PartCluster()
  /* constructor with input # partitiions */
  { _tail = 0; _toler = 0.04; _tolerKL = 0.25;}

  int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid);
  /* partitiion the init geometry to grid */

  int adjust_bisect(BlockGraph& grid, int pid0, int pid1);
  /* adjust between two partitions */

  private: //-------  private member functions  ----------//
  int _init_shmem_map(BlockGraph& bg);
  /* init all b2b maps as shared memory */

  int _grow_bisect(BlockGraph& bg, int pid, double ratio);
  /* bisect via graph growing */

  int _seed(BlockGraph& bg, int pid);
  /* return the seed block id */

  int _exchange(BlockGraph& bg, int pid0, int pid1);
  /* exchange elementary blocks between given partitions */

  int _move_blk(BlockGraph& bg, int bid, int pid0, int pid1);
  /* move block id from part0 to part1 */

  private: //---------- private attributes      ----------//
  double         _tolerKL;
  int            _tail;    // tail of used parts
  PartArray      _parts;
  ElementHeap    _hp0, _hp1;
  vector<bool>   _isMvds;
  vector<double> _msgDiffs0, _msgDiffs1; // msg to other part - msg to self
};

#endif
