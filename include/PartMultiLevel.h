#ifndef PARTMULTILEVEL_H
#define PARTMULTILEVEL_H

#include "common.h"
#include "Block.h"
#include "BlockGraph.h"
#include "PartMethod.h"
#include <metis.h>

class PartMultiLevel : public PartMethod
{
  public:  //---------- public member functions ----------//
  PartMultiLevel()
  /* default constructor */
  { _unitRatio = 1.0/4.0; }

  int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid);

  int cut_large_block(BlockGraph& bg, vector<int>& bids);
  
  int view_metis_args();

  private:  //----------   private functions   ----------//
  int _setup_metis_args(BlockGraph& bg);

  int _clear_metis_args();

  int _cut_blks_to_units(BlockGraph& bg);

  private:  //----------   private attributes  ----------//
  double  _unitRatio; // ratio between avg and unit block
  idx_t   _nVtx;      // # vertices 
  idx_t   _nEdge;     // # edges
  idx_t   _nCon;      // # of balance constraints, i.e. vertex weight
  idx_t*  _xadjs;     // target vertex index starts
  idx_t*  _vwgts;     // vertex weight
  idx_t*  _adjncys;   // target vertex id
  idx_t*  _adjwgts;   // edge weight
  idx_t*  _partIDs;   // partition indices
  real_t* _tpwgts;    // weight of constraint
  real_t* _ubvec;     // tolerance
  idx_t   _options[METIS_NOPTIONS];
};

#endif
