/*------------------------------------------------------------------------------
Purpose:
  Define BlockGraph class to hold blocks.

Discription:
  A vector container is used to hold blocks. Each block has the map info to
  other blocks. So the class is essentially a graph.

Author:
  Hengjie Wang, hengjiew@uci.edu
------------------------------------------------------------------------------*/

#ifndef BLOCKGRAPH_H
#define BLOCKGRAPH_H

#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>
#include <assert.h>
#include "Block.h"
#include "GridInput.h"
#include "cgnslib.h"
#include "mpi.h"


struct conn_data {
    char conn_name[33];
    char donor_name[33];
    int transform[3];
    cgsize_t pnts[2][3];
    cgsize_t pntsdonor[2][3];
};

struct bc_data{
    char bc_name[33];
    CGNS_ENUMT(BCType_t) bc_type;
    CGNS_ENUMT(PointSetType_t) set_type;
    CGNS_ENUMT(DataType_t) normaldatatype;
    cgsize_t npnts;
    cgsize_t normallistflag;
    cgsize_t pnts[2][3];
    int normallist;
    int normal_index[3];
    int ndataset;
};

struct block_data {
    double *x;
    double *y;
    double *z;
    int zone_index;
    cgsize_t size[3][3];
    string block_name;
    bool block_valid;
    int conns_count;
    int bcs_count;
    conn_data *conns[30];
    bc_data *bocos[30];
};

class cgns_write_test {
    public:
    cgns_write_test() {file_opened = false;};
    ~cgns_write_test() {file_opened = false;};

    void fwrite_open(string file_name, int baseNum, string base_name);
    void fwrite_block(int x_start, int x_end, int y_start, int y_end, int z_start, int z_end, string zone_name);
    void fwrite_block(int *rngs, Block &blks, string zone_name, bool isWriteCoord = true);
    void fwrite_connectivity(string conn_name, string zonename1, int z1info[5],  string zonename2, int z2info[5], bool isFlip);

    void fwrite_bc(string zonename, string boconame, int face, int lower_pnts[2], int higher_pnts[2], string bcType);
    void fwrite_close();

    private:
    int find_block_by_name(string zonename);
    char file_name[30];
    char base_name[30];
    int index_file;
    int index_base;
    int icelldim;
    int iphysdim;
    bool file_opened;
};


class cgns_read_test {
    public:
    cgns_read_test() { file_opened = false;};
    ~cgns_read_test() { file_opened = false;};

    void fread_open(string file_name, string base_name);
    int fread_blkcount() {return this -> nBlk;}
    block_data fread_block(string block_name);
    void fread_blocks(int indexFile, int indexBase, vector<Block>  &blks, bool isReadCoord = true);
    void fread_connectivity(string block_name, block_data &block);
    void fread_connectivity_with_index(int index_zone);
    void fread_bc(string zonename, block_data &block);
    void fread_close();

    private:
    char file_name[30];
    char base_name[30];
    int index_file;
    int index_base;
    int icelldim;
    int iphysdim;
    int nBlk;
    bool file_opened;

};

class BlockGraph
{
  public:  //---------- public member functions ----------//

  // basics
  BlockGraph()
    { alpha = 6.7489e-5; beta = 1.0683e8; sizeCellMsg = 1; nHalo = 2; _tPerCell = 5.0e-8; }
  BlockGraph(const int nBlock);
  BlockGraph(const BlockGraph& rhs);
  const BlockGraph& operator=(const BlockGraph& rhs);

  int size() const
    { return((int)blks.size()); }

  int pop_back()
    { blks.pop_back(); return(0); }

  Block& operator[](int i);

  void get_block_pt(int blkID, Block** ptBlk);

  void get_comm_params(double& ltncy, double& bndwid, int& sizeCell) const
  { ltncy = alpha; bndwid = beta; sizeCell = sizeCellMsg; }

  double time_per_cell() const
  /* return computation time per cell */
  { return(_tPerCell); }

  int num_halo() const
  /* return number of halo */
  { return(nHalo); }
  
  int work_load();

  double work();

  int resize(int nBlock);
  /*----------------------------------------------------------
    reset # blocks

    In:   nBlock  -  # blocks

    Mthd: Remove all all blocks and add new empty blocks.
  ----------------------------------------------------------*/

  int view();
  /*----------------------------------------------------------
    Call each block's view function
  ----------------------------------------------------------*/

  int copy_coord(const BlockGraph& prntBg);
  /*----------------------------------------------------------
    Copy coordinates from parent BG to current BG

    In:   prntBg  -  parent block graph

    Mthd: Each sub block find its parent block and copy the
          coordinates.
  ----------------------------------------------------------*/

  int fread_mesh_plot3d(const string& fname, bool isReadCoord = true);
  /*----------------------------------------------------------
    Read coordinates from a ascii file in plot3d format

    Note: The coordinates in block is stored along k index
  ----------------------------------------------------------*/

  int fread_map(const string& fname);
  /* Read bc from a ascii file */

  int fread_cgns(string fname, string base = "Partition", bool isReadCoord = true);
  /* Read coordinates from a CGNS file */

  int read_input(const GridInput& input);
  /* init via GridInput object */

  int fwrite_mesh_tecplot(const string& fname);
  /*----------------------------------------------------------
    The coordinates and partID to a ascii file using tecplot'
    contour file format.
  ----------------------------------------------------------*/

  int setup_msg_load(double alpha, double beta, int sizeCellMsg);

  void set_cell_time(double tPerCell)
  { _tPerCell = tPerCell; }
  /* ser computation time per cell */

  void set_num_halo(int numHalo)
  { nHalo = numHalo; }

  double latency()
  { return(alpha); }

  double bandwidth()
  { return(beta); }

  int cell_data_size()
  { return(sizeCellMsg); }

  int cmpt_msg_load(int bid);

  int cmpt_time(int bid);
  /* compute the time cost of input block */

  int cmpt_nbr_time(int bid);
  /* compute the time cost of neighbors of the input block */

  double time_comm(int bid, int fid, int pos, \
                   const vector<int>& cmpnys = VECTOR_INT_NULL);
  /* compute the communication time of input block */

  double time_comp(int bid) const
  /* return computation time of given block */
  { return(blks[bid].work_load() * _tPerCell); }

  int reset_parent();

  int find_min_blkcut(int blkID, double load, double err, BlockCut& cut, \
                      const vector<int>& cmpnys = VECTOR_INT_NULL);
  /*----------------------------------------------------------
    Find the cut within given work load which minimizes commucation.

    In:  blkID  -  block id
         load   -  work load to cut off (#cells)
         err    -  tolerance
         cmpnys -  optional, blocks already in same parition
    Out: cut    -  block cut
  ----------------------------------------------------------*/

  int find_time_cut(int blkID, double timeLoad, double err, BlockCut& cut, \
                    const vector<int>& cmpnys = VECTOR_INT_NULL);
  /*----------------------------------------------------------
    Find the cut within given time load which minimizes commucation.

    In:  blkID  -  block id
         load   -  time load to cut off
         err    -  tolerance
         cmpnys -  optional, blocks already in same parition
    Out: cut    -  block cut
  ----------------------------------------------------------*/

  int find_min_blkdiv(int bid, int nPart, int n[3], double& tCommMax);
  /* find the division of given block and max comm time of new blocks */

  int find_min_blkdiv(BlockCut& cut, int nPart, int n[3], double& tCommMax);

  int cut_block(BlockCut& cut);
  /* cut a block by a given cut */

  int cut_block(int bid, vector< vector<int> >& cutLocs);
  /* cut a block by given positions in each direction */

  int cut_block(int bid, int nSubBlk);
  /* cut a block to a given number of sub blocks */

  int reb(int bid, int npREB, double toler);

  int factorize_block(int bid, vector<int>& preBids, int np, double toler);

  int find_time_bisect(int bid, double ratio, double err, BlockCut& cut);
  /* find a bisection according to the input ratio */

  int cut_to_elements();
  /* cut all blocks into elements */

  list<BcMap>::iterator inv_map_iter(const BcMap& map);

  int add_inv_map(int blkID, int fcID, const BcMap& map);
  /* add a inverse map of input map */
  
  int del_inv_map(int blkID, int fcID, const BcMap& map);
  /* delete a inverse map of input map */

  double msg_load(int blkID0, int blkID1);
  /* Return the msg load between blk0 and blk1 */

  double msg_load(const BlockCut& cut, int bid);
  /* Return the msg load between cut and block bid */

  int set_shmem_map(int nBlk, int *blkIDs);
  /* set the map between given blocks through shared mem */

  int cmpt_msg_incr(BlockCut& cut, int toPart);
  /* Cmpt the msg incr caused by moving the cut-off */

  int cmpt_msg_incr(BlockCut& cut, const vector<int>& cmpnys = VECTOR_INT_NULL);
  /* Cmpt the msg incr caused by performing the cut */

  double msg_to_part(int blkID, int fcID, int toPart);

  double msg_to_part(int blkID, int toPart);
  
  BlockGraph block_coalesce(BlockGraph &returnGraph);

  int merge(int bid, int nCmpy = 0, int *cmpys = 0);

  int merge_map(int bid);

  int fwrite_block_info(const string& fname);
  /* write to file the range, partID, prntID of block */

  int fwrite_map(const string& fname);
  /* write to file the content of mapfile */

  // int fwrite_cgns(const string& fname);

  int fwrite_cgns(const string& fname, bool isWriteCoord = true);

  int fwrite_block_profile(const string& fname);
  /* write to file the size distribution of block */

  int debug_check();
  /*----------------------------------------------------------
    Check the following things:
    * length of block is at least nHalo
    * each b2b map has inverse map
    * Maps don't intersect and cover whole face
  ----------------------------------------------------------*/

  int set_mesh(int nBlock, const int* il, const int* jl, const int* kl);
  int read_bc(int* NbcInt, int length, string* bcString, int* bcInt);
  void get_ID(int np, int* partiID);
  void get_bc_name(string* bcstring);
  void get_number_faces(int &numberoffaces, int* nsurface);
  void get_bc(int* bclist);
  void get_range(int* range);

  private: //---------- private functions       ----------//
  int _find_best_cuts(int bid, int nSubBlk, int& nSingleCut, int nDivs[3]);
  /*----------------------------------------------------------
    find the cuts of a single block which minimize communication
    
    Note: * this does not take in account the commucation on
            block boundary
          * nSubBlk = d + a*b*c;

    In:   bid        - block id to be worked on
          nSubBlk    - # blocks to be cut into
    Out:  nSingleCut - d
          nDivs[3]   - a, b, c
  ----------------------------------------------------------*/

  int _find_blk_division(int lens[3], int nSubBlk, int nDivs[3], double &msg);
  /*----------------------------------------------------------
    Given the length and # sub blocks of a block, find the
    division that minimized the overall communication.
    
    In:  lens    - block's length in 3 directions
         nSubBlk - # sub blocks
    Out: nDivs   - # of division in 3 directions
         msg     - communication time
  ----------------------------------------------------------*/

  int _bisect_to_1_block(int bid, int npREB, double toler, vector<int>& npNewBlks);

  private: //---------- private attributes      ----------//
  double         alpha, beta; // alpha-beta model for comm
  int            sizeCellMsg; // # data * sizeofdata per cell
  int            nHalo;       // halo for b2b
  double         _tPerCell;   // comp time per cell
  int            nBlk;        // number of blocks in blks. Read from CGNS
  vector<Block>  blks;

  friend class BlockHeap;
  friend class BlockPart;
  friend class Partition;
};

#endif
