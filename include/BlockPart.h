#ifndef BLOCKPART_H
#define BLOCKPART_H

#include <cmath>
#include "Block.h"
#include "BlockGraph.h"
#include "BlockHeap.h"
#include "Partition.h"
#include "PartMethod.h"

const int MAX_BLK_PER_PROC = 8;

class BlockPart : public PartMethod
{
  public:  //---------- public member functions ----------//
  BlockPart()
  { tolerRB = 0.04; }

  // basic setters
  void set_bisect_toler(double val)
    { tolerRB = val; }

  int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid);

  int grow_blks_to_part(BlockGraph& grid, vector<int>& bids, int id);
  /*----------------------------------------------------------
    Grow from a small block whose work load is less than average.
    Keep adding small block or cut-off until the partition is fulfilled.
   
    In:  grid  - blockGraph worked on
         bids  - block ids in the graph worked on
         id    - block ids already in one part

    Res: The part id of blocks/cut-offs in that part is set.
  ----------------------------------------------------------*/

  int bisect_blk_to_parts(BlockGraph& grid, vector<int>& bids, int id);
  /*----------------------------------------------------------
    Bisect a larget block to cut-offs and each cut-off is assigned
    with a partition id.
   
    In:  grid  - blockGraph worked on
         id    - id of block to cut

    Res: Each cut-off's partition id is set.
  ----------------------------------------------------------*/

  int find_min_cmpny(BlockGraph& bg, vector<int>& bids, vector<int>& localBids, int& cmpny);
  /*----------------------------------------------------------
    Find a block or cut-off to accompany blocks already in one
    partition so they fit in the work load per partition.
   
    In:  bg        - blockGraph worked on
         bids      - block ids in the graph worked on
         localBids - block ids already in one part
    Out: cmpny     - block id of the company
  ----------------------------------------------------------*/

  int _bisect_blk(BlockGraph& bg, vector<int>& bids, int blkID, int nProc);
  /*----------------------------------------------------------
    Bisect block 'blkID' to 'nProc' partitions.
   
    In:  bg     - blockGraph worked on
         bids   - block ids in the graph worked on
         blkID  - block id to be bisected
         nProc  - # partitions

    Res: 'nProc-1' blks are appended to 'bg' and 'bids'.
  ----------------------------------------------------------*/

  int _bisect_to_one_part(BlockGraph& bg, int blkID, int nProc, vector<int>& npNewBlks);
  /*----------------------------------------------------------
    Bisect block 'blkID' until the in place block fits in one
    partition.
   
    In:  bg        - blockGraph worked on
         blkID     - block id to be bisected
         nProc     - # partitions
    Out: npNewBlks - # partitions for new added blocks

    Res: New blocks are appended to 'bg'. Its work load is saved
         in 'npNewBlks'.
  ----------------------------------------------------------*/

  int update_heap_nbr(BlockGraph& bg, int bid);

  private: //---------- private attributes      ----------//
  int               iPart;       // the index of part to assign
  double            tolerRB;     // tolerance of imbalance in RB
  BlockHeap         hp;          // block heap
  vector<bool>      isDones;     // is block assigned
};

#endif
