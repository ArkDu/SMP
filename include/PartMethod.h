#ifndef PARTMETHOD_H
#define PARTMETHOD_H

#include "common.h"
#include "Block.h"
#include "BlockGraph.h"

enum Method_t {PureGreedy, GreedyCut, GreedyZone, GraphGrow, MultiLevel, PureGreedyCut};

class PartMethod
{
  public:  //---------- public member functions ----------//
  virtual int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid) =0;
  /*----------------------------------------------------------
    virtual function, decompose blocks to partitions.
    
    In:  geo   - geometry block graph
         bids  - block ids to be worked on
         nPart - # partitions
    Out: grid  - grid blocks graph
  ----------------------------------------------------------*/

  void set_tolerance(double toler)
  /* set the tolerance of error in work load per partition. */
  { _toler = toler; }

  int shift_partition_id(BlockGraph& grid, vector<int> bids, int iShift);
  /*----------------------------------------------------------
    shift the parition id of given blocks. This is useful when
    in hybrid partitioning.
    
    In:  grid   - grid block graph
         bids   - block ids to be worked on
         iShift - shift of index

    Pre: The partition id of bids in grid must aleady be set.
  ----------------------------------------------------------*/

  protected: //----------   private functions   ----------//
  int    _nPart;
  double _toler; // tolerance for partitions
  double _wkAvg;
};

#endif
