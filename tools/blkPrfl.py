import os
import sys
#sys.path.append("/usr/lib64/python3.6/site-packages")
import numpy as np
import matplotlib.pyplot as plt

argc = len(sys.argv)
nBlkTot  = [];
wkMax    = 0.0;
wkMin    = 1.0e14;

for i in range(argc-1):
  with open(sys.argv[i+1], 'r') as infile:
    # get total # blocks
    nBlkTot.append(infile.readline()[1])
    # skip title line
    infile.readline()
    # read work load, find max & min
    while True:
      line = infile.readline()
      if not line:
        break
      line = line.split()
      if wkMin > float(line[1]):
        wkMin = float(line[1])
      if wkMax < float(line[1]):
        wkMax = float(line[1])

# set the intervals
nIntv = 20;
step  = (wkMax - wkMin) / nIntv;
xIntvs = [wkMin + (j+0.5)*step for j in range(nIntv)]

# count blocks in interval
blkPrfls = []; 
for i in range(argc-1):
  blkPrfls.append([0 for j in range(nIntv)]);
  with open(sys.argv[i+1], 'r') as infile:
    for j, line in enumerate(infile):
      # skip two lines, read block workload
      if j > 1:
        line = line.split()
        iIntv = min(np.int(np.floor((float(line[1]) - wkMin)/step)), nIntv-1)
        blkPrfls[i][iIntv] = blkPrfls[i][iIntv] + 1;

# plot the blocks' workload
fnames = []
for i in range(argc-1):
  x = []
  y = []
  for j in range(nIntv):
    if blkPrfls[i][j] > 0:
      x.append(xIntvs[j])
      y.append(blkPrfls[i][j])
  plt.plot(x, y, 'o')
  fnames.append(sys.argv[i+1][:-4])
plt.xlabel('work load')
plt.ylabel('#blocks')
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.legend(fnames)
plt.savefig("tmp.pdf") 
