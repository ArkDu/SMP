import os
import sys

imblncMaxs = []
commVolMaxs= []
edgeCutMaxs= []
commVolAvgs= []
edgeCutAvgs= []
nBlks      = []
tCommMax   = []
tCommAvg   = []

with open(sys.argv[1]) as infile:
  for i, line in enumerate(infile):
    items = line.split()
    if(len(items) > 1):
      if items[0] == "Imbalance:":
        imblncMaxs.append(items[2])
      if items[0] == "Edge":
        edgeCutMaxs.append(items[3])
        edgeCutAvgs.append(items[2])
      if items[0] == "Comm":
        commVolMaxs.append(items[3])
        commVolAvgs.append(items[2])
      if items[0] == "#blocks":
        nBlks.append(items[1])
      if (len(items) > 2 and items[1] == "Comm:"):
        tCommMax.append(items[3])
        tCommAvg.append(items[2])

print("imbalance")
for i in range(len(imblncMaxs)):
  print(imblncMaxs[i])
print("edgeCutMax")
for i in range(len(imblncMaxs)):
  print(edgeCutMaxs[i])
print("edgeCutAvg")
for i in range(len(imblncMaxs)):
  print(edgeCutAvgs[i])
print("commVolMax")
for i in range(len(imblncMaxs)):
  print(commVolMaxs[i])
print("commVolAvg")
for i in range(len(imblncMaxs)):
  print(commVolAvgs[i])
print("#blocks")
for i in range(len(imblncMaxs)):
  print(nBlks[i])
print("TimeCommMax")
for i in range(len(imblncMaxs)):
  print(tCommMax[i])
print("TimeCommAvg")
for i in range(len(imblncMaxs)):
  print(tCommAvg[i])
