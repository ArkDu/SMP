#include <iostream>
#include "BlockGraph.h"

using namespace std;

const int Nx = 128;
const int Ny =  64;
const int Nz =  32;

int main()
{
  int rngs[6];
  BlockGraph bg(1);

  Block *ptb;
  bg.get_block_pt(0, &ptb);

  // set block data
  rngs[0] = 0;  rngs[3] = Nx;
  rngs[1] = 0;  rngs[4] = Ny;
  rngs[2] = 0;  rngs[5] = Nz;
  ptb->set_range(rngs);

  // set block bc
  // x- bc
  rngs[0] = 0;  rngs[3] =  0;
  rngs[1] = 0;  rngs[4] = Ny;
  rngs[2] = 0;  rngs[5] = Nz;
  ptb->add_bc(0, "EulerWall", rngs);
  // x+ bc
  rngs[0] = Nx;  rngs[3] = Nx;
  rngs[1] =  0;  rngs[4] = Ny;
  rngs[2] =  0;  rngs[5] = Nz;
  ptb->add_bc(3, "EulerWall", rngs);
  // y- bc
  rngs[0]   = 0; rngs[3]   = Nx;
  rngs[1]   = 0; rngs[4]   =  0;
  rngs[2]   = 0; rngs[5]   = Nz;
  ptb->add_bc(1, "EulerWall", rngs);
  // y+ bc
  rngs[0] =  0; rngs[3] = Nx;
  rngs[1] = Ny; rngs[4] = Ny;
  rngs[2] =  0; rngs[5] = Nz;
  ptb->add_bc(4, "EulerWall", rngs);
  // z- bc
  rngs[0] =  0;  rngs[3] = Nx;
  rngs[1] =  0;  rngs[4] = Ny;
  rngs[2] =  0;  rngs[5] =  0;
  ptb->add_bc(2, "EulerWall", rngs);
  // z- bc
  rngs[0] =  0;  rngs[3] = Nx;
  rngs[1] =  0;  rngs[4] = Ny;
  rngs[2] = Nz;  rngs[5] = Nz;
  ptb->add_bc(5, "EulerWall", rngs);

  bg.cut_block(0, 17);
  bg.view();

  return(0);
}
