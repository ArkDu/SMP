#include "Partitioner.h"

/************************      Partitioner Class       ************************/

void Partitioner::setup_partition_method(Method_t mthdType)
{
  _mthdType = mthdType;

  switch(mthdType){
    case GreedyCut:
      _ptPart = new PartGreedyCut;
      break;
    case GreedyZone:
      _ptPart = new PartGreedZone;
      break;
    case GraphGrow:
      _ptPart = new PartCluster;
      break;
    case MultiLevel:
      _ptPart = new PartMultiLevel;
      break;
    case PureGreedy:
      _ptPart = new PartPureGreedy;
      break;
    case PureGreedyCut:
      _ptPart = new PartPureGreedyCut;
      break;
  }

  _ptPart->set_tolerance(_toler);
}


int Partitioner::setup_partition(BlockGraph& grid)
{
  // reset partition array
  _parts.clear();

  // add block to its partition
  for(int i=0; i<grid.size(); i++){
    int pid = grid[i].partition_id(); 
    _parts[pid].blkIDs.push_back(i);
    _parts[pid].add_load((double)grid[i].work_load());
  }

  // set up each partition's neighbor info
  for(int i=0; i<_nPart; i++)
    _parts[i].setup_nbr(grid);

  for(int i=0; i<_nPart; i++)
    _parts[i].cmpt_time(grid);

  return(0);
}


int Partitioner::decompose(BlockGraph& geo, BlockGraph& grid)
{
  vector<int> bids;

  // average work load
  double wkTot = 0.0;
  for(int i=0; i<geo.size(); i++)
    wkTot += (double)geo[i].work_load();
  _wkAvg = wkTot / _nPart;

  // include all geometry block to block ids
  for(int i=0; i<geo.size(); i++)
    bids.push_back(i);

  _ptPart->decompose(geo, bids, _nPart, grid);

  setup_partition(grid);

  // merge blocks
  _parts.merge_part_blk(grid);

  // setup shared memory map
  for(int i=0; i<_nPart; i++){
    grid.set_shmem_map((int)_parts[i].blkIDs.size(), &(_parts[i].blkIDs[0]));
  }

  // update block and partitions's time cost
  for(int i=0; i<grid.size(); i++)
    grid.cmpt_time(i);
  for(int i=0; i<_nPart; i++)
    _parts[i].cmpt_time(grid);

  // sort partitions
  _parts.build();

  return(0);
}


int Partitioner::fwrite_part_time(const string& fname)
{
  // write to file
  ofstream output(fname);
  output << "part\t#Blocks\tWork\tTimeTotal\tTimeComp\tTimeComm\tnComm"
         << "\tvolComm\tnCopy\tvolCopy\n";
  for(int i=0; i<_nPart; i++)
    output << i << "\t" << _parts[i].blkIDs.size() << "\t" << _parts[i].work() 
           << "\t" << _parts[i].time() << "\t" << _parts[i].time_comp() << "\t"
           << _parts[i].time_comm() << "\t" << _parts[i].num_comm() << "\t" 
           << _parts[i].vol_comm() << "\t" << _parts[i].num_shmem_copy() << "\t" 
           << _parts[i].vol_shmem_copy() << endl;
  output.close();

  return(0);
}


int Partitioner::view_part()
{
  cout << "--------" << "Partitions" << "--------" << endl;
  cout << " Average work load: " << _wkAvg << endl;
  for(int i=0; i<_nPart; i++)
    _parts.rank(i).view();

  return(0);
}


int Partitioner::view_part(BlockGraph& bg, int pid)
{
  double wkPart = 0.0;
  double volShr = 0.0, volComm = 0.0;
  int    nCpy   = 0,   nComm   = 0;

  for(uint i=0; i<_parts[pid].blkIDs.size(); i++){
    int bid = _parts[pid].blkIDs[i];
    wkPart += bg[bid].work();
    for(int j=0; j<6; j++){
      BlockFace *ptFc = bg[bid].face_pt(j);
      CMapIter   iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0){
          double vol = max(iter->rngs[5] - iter->rngs[2], 1) \
                     * max(iter->rngs[4] - iter->rngs[1], 1) \
                     * max(iter->rngs[3] - iter->rngs[0], 1) \
                     * (double)bg.num_halo() * bg.cell_data_size();
          if(iter->isShMem){
            nCpy++;
            volShr += vol;
          }
          else{
            nComm++;
            volComm += vol;
          }
        }
        iter++;
      }
    }
  }

  cout << "Partition " << pid << ":\n"
       << "--#Blocks     " << _parts[pid].blkIDs.size()
       << "--workLoad    " << wkPart << "\n"
       << "--EdgeCut     " << nComm << " Volume     " << volComm << "\n"
       << "--EdgeCut Shr " << nCpy  << " Volume Shr " << volShr << endl;
  for (uint i=0; i<_parts[pid].blkIDs.size(); ++i) 
    bg[_parts[pid].blkIDs[i]].view();
  
  return(0);
}


int Partitioner::eval_part_prop(BlockGraph& bg)
{
  fill(_edgeCuts, _edgeCuts+4, 0.0);  _edgeCuts[2] = DLARGE;
  fill(_commVols, _commVols+4, 0.0);  _commVols[2] = DLARGE;
  fill(_imblncs,  _imblncs +4, 0.0);  _imblncs[2]  = DLARGE;
  fill(_t,        _t+4,        0.0);  _t[2]        = DLARGE;
  fill(_tComp,    _tComp+4,    0.0);  _tComp[2]    = DLARGE;
  fill(_tComm,    _tComm+4,    0.0);  _tComm[2]    = DLARGE;
  fill(_directCpyVols,   _directCpyVols+4,   0.0);
  _directCpyVols[2] = DLARGE;
  fill(_indirectCpyVols, _indirectCpyVols+4, 0.0);
  _indirectCpyVols[2] = DLARGE;

  // # blocks
  _nBlk = bg.size();

  // re-compute avg work
  double wkTot = 0.0;
  for(int i=0; i<bg.size(); i++)
    wkTot += bg[i].work();
  _wkAvg = wkTot / _nPart;

  for(int i=0; i<_nPart; i++){
    double edgeCut = 0.0, commVol = 0.0, wkload = 0.0, imblnc;

    for(unsigned int j=0; j<_parts[i].blkIDs.size(); j++){
      int id = _parts[i].blkIDs[j];
      edgeCut += bg[id].latency_load();
      commVol += bg[id].msg_load() - bg[id].latency_load();
      wkload  += (double)bg[id].work_load();
    }
    edgeCut = edgeCut / bg.latency();
    commVol = commVol * bg.bandwidth();

    // eval _edgeCuts
    _edgeCuts[0] += edgeCut;
    _edgeCuts[1]  = max(_edgeCuts[1], edgeCut);
    _edgeCuts[2]  = min(_edgeCuts[2], edgeCut);
    _edgeCuts[3] += edgeCut * edgeCut;

    // eval commVol
    _commVols[0] += commVol;
    _commVols[1]  = max(_commVols[1], commVol);
    _commVols[2]  = min(_commVols[2], commVol);
    _commVols[3] += commVol * commVol;

    // eval imbalance
    imblnc = (wkload - _wkAvg) / _wkAvg;
    _imblncs[0]  += imblnc;
    _imblncs[1]   = max(_imblncs[1], imblnc);
    _imblncs[2]   = min(_imblncs[2], imblnc);
    _imblncs[3]  += imblnc * imblnc;

    // time
    _t[0]     += _parts[i].time();
    _t[1]      = max(_t[1], _parts[i].time());
    _t[2]      = min(_t[2], _parts[i].time());
    _t[3]     += _parts[i].time() * _parts[i].time();
    //..
    _tComp[0] += _parts[i].time_comp();
    _tComp[1]  = max(_tComp[1], _parts[i].time_comp());
    _tComp[2]  = min(_tComp[2], _parts[i].time_comp());
    _tComp[3] += _parts[i].time_comp() * _parts[i].time_comp();
    //..
    _tComm[0] += _parts[i].time_comm();
    _tComm[1]  = max(_tComm[1], _parts[i].time_comm());
    _tComm[2]  = min(_tComm[2], _parts[i].time_comm());
    _tComm[3] += _parts[i].time_comm() * _parts[i].time_comm();

    // copy volume for communication
    _directCpyVols[0]   += _parts[i].vol_direct_copy();
    _directCpyVols[1]    = max(_directCpyVols[1], _parts[i].vol_direct_copy());
    _directCpyVols[2]    = min(_directCpyVols[2], _parts[i].vol_direct_copy());
    _directCpyVols[3]   += _parts[i].vol_direct_copy() * _parts[i].vol_direct_copy();
    _indirectCpyVols[0] += _parts[i].vol_indirect_copy();
    _indirectCpyVols[1]  = max(_indirectCpyVols[1], _parts[i].vol_indirect_copy());
    _indirectCpyVols[2]  = min(_indirectCpyVols[2], _parts[i].vol_indirect_copy());
    _indirectCpyVols[3] += _parts[i].vol_indirect_copy() * _parts[i].vol_indirect_copy();

    // share memory copy volume
    _shrVols[0] += _parts[i].vol_shmem_copy();
    _shrVols[1]  = max(_shrVols[1], _parts[i].vol_shmem_copy());
    _shrVols[2]  = min(_shrVols[2], _parts[i].vol_shmem_copy());
    _shrVols[3] += _parts[i].vol_shmem_copy() * _parts[i].vol_shmem_copy();
  }

  // average
  _edgeCuts[0] = _edgeCuts[0] / _nPart;
  _commVols[0] = _commVols[0] / _nPart;
  _imblncs[0]  = _imblncs[0]  / _nPart;
  _t[0]        = _t[0]        / _nPart;
  _tComm[0]    = _tComm[0]    / _nPart;
  _tComp[0]    = _tComp[0]    / _nPart;
  _shrVols[0]  = _shrVols[0]  / _nPart;
  _directCpyVols[0]   = _directCpyVols[0]   / _nPart;
  _indirectCpyVols[0] = _indirectCpyVols[0] / _nPart;

  // standard deviation
  _edgeCuts[3] = sqrt(_edgeCuts[3]/_nPart - _edgeCuts[0]*_edgeCuts[0]);
  _commVols[3] = sqrt(_commVols[3]/_nPart - _commVols[0]*_commVols[0]);
  _imblncs[3]  = sqrt( _imblncs[3]/_nPart -  _imblncs[0]* _imblncs[0]);
  _t[3]        = sqrt(       _t[3]/_nPart -        _t[0]*_t[0]);
  _tComp[3]    = sqrt(   _tComp[3]/_nPart -    _tComp[0]*_tComp[0]);
  _tComm[3]    = sqrt(   _tComm[3]/_nPart -    _tComm[0]*_tComm[0]);
  _shrVols[3]  = sqrt( _shrVols[3]/_nPart -  _shrVols[0]*_shrVols[0]);
  _directCpyVols[3]   = sqrt( _directCpyVols[3]/_nPart -  _directCpyVols[0]*_directCpyVols[0]);
  _indirectCpyVols[3] = sqrt( _indirectCpyVols[3]/_nPart -  _indirectCpyVols[0]*_indirectCpyVols[0]);

  return(0);
}


int Partitioner::view_part_prop()
{
  // set method's name
  string mthdStr = "puregreedy";
  switch(_mthdType){
    case GreedyCut:
      mthdStr = "greedycut"; break;
    case GreedyZone:
      mthdStr = "greedyzone"; break;
    case PureGreedy:
      mthdStr = "puregreedy"; break;
    case PureGreedyCut:
      mthdStr = "greedy + grow"; break;
  }

  // print
  cout << "----------------" << endl;
  cout << "Partition Method: " << mthdStr << endl;
  cout << "Generate " << _nPart << " partitions." << endl;
  cout << "Partition Properties: avg | max | min | sd" << endl;
  cout << "Imbalance: ";
  for(int i=0; i<4; i++)
    cout << _imblncs[i] << " ";
  cout << endl;
  cout << "Edge Cut:  ";
  for(int i=0; i<4; i++)
    cout << _edgeCuts[i] << " ";
  cout << endl;
  cout << "Comm Vol:  ";
  for(int i=0; i<4; i++)
    cout << _commVols[i] << " ";
  cout << endl;
  cout << "DCpy Vol:  ";
  for(int i=0; i<4; i++)
    cout << _directCpyVols[i] << " ";
  cout << endl;
  cout << "ICpy Vol:  ";
  for(int i=0; i<4; i++)
    cout << _indirectCpyVols[i] << " ";
  cout << endl;
  cout << "SCpy Vol:  ";
  for(int i=0; i<4; i++)
    cout << _shrVols[i] << " ";
  cout << endl;
  cout << "Time:      ";
  for(int i=0; i<4; i++)
    cout << _t[i] << " ";
  cout << endl;
  cout << "Time Comp: ";
  for(int i=0; i<4; i++)
    cout << _tComp[i] << " ";
  cout << endl;
  cout << "Time Comm: ";
  for(int i=0; i<4; i++)
    cout << _tComm[i] << " ";
  cout << endl;
  cout << "#Blocks: " << _nBlk << endl;

  return(0);
}


int Partitioner::adjust_all(BlockGraph& bg)
{
  BlockCut cut;
  int      toPart;

  // set ideal work for all parts
  for(int i=0; i<_nPart; i++)
    _parts[i].set_ideal_work(_wkAvg);

  // build the partition-shift table
  _tbl.clear();
  for(int i=_nPart-1; i>=0; i--){// part
    // overload parition
    if(_parts.rank(i).work() >= _wkAvg*(1.0+_toler)){// overload
      // create new row
      _tbl.append_row(_parts.rank(i).id);
      // traverse partition's nbr
      for(int j=0; j<_nPart; j++){// nbr
        if(_parts.rank(j).work() > _wkAvg)  break;
        int pidFrom = _parts.rank(i).id;
        int pidTo   = _parts.rank(j).id;
        _parts.find_part_shift(bg, pidFrom, pidTo, _toler, cut);
        if(cut.blkID != BLOCK_NULL)  _tbl.add(pidFrom, pidTo, cut);
      }// end nbr loop
    }// end overload if
  }// end part loop
#if verbose>=1
  cout << "---------------------------" << endl;
  cout << "Partitioner: Build up table" << endl;
#endif

  // shift cut-off/block according to table
  int      pid;
  BlockCut cutTmp;
  while(!_tbl.is_empty()){// _tbl
    _tbl.pop(pid, toPart, cut);
    // move cut-off or block to target
    _parts.shift_part(bg, toPart, cut);
    // delete pid's row if pid is within toler after shift
    if(_parts[pid].work() <= _wkAvg*(1.0+_toler)){
      _tbl.del_row(pid);
#if verbose>=1
      cout << "BlockPart: " << pid << " fit in toler, ";
#endif
    }
    // reset row if pid is still overload
    else{
      _tbl.clear_row(pid);
      for(int j=0; j<_nPart; j++){
        // only traverse underload parts
        if(_parts.rank(j).work() > _wkAvg)  break;
        int pidTo = _parts.rank(j).id;
        _parts.find_part_shift(bg, pid, pidTo, _toler, cutTmp);
        if(cutTmp.blkID != BLOCK_NULL)  _tbl.add(pid, pidTo, cutTmp);
      }
#if verbose>=1
    cout << "Partitioner: " << pid << " still overload, ";
#endif
    }
    // delete from all rows involving toPart
    _tbl.del_target(toPart);
    // if toPart still underload, update part shift and add to table
    if(_parts[toPart].work() < _wkAvg){
      for(int j=_nPart-1; j>=0; j--){
        if(_parts.rank(j).work() <= _wkAvg*(1.0+_toler)) break;
        int pidFrom = _parts.rank(j).id;
        _parts.find_part_shift(bg, pidFrom, toPart, _toler, cutTmp);
        if(cutTmp.blkID != BLOCK_NULL)  _tbl.add(pidFrom, toPart, cutTmp);
      }
#if verbose>=1
      cout << "update " << toPart << " nbrs." << endl;
#endif
    }
    // do nothing
    else{
#if verbose>=1
      cout << "remove " << toPart << " nbrs." << endl;
#endif
    }
  }// _tbl loop

  // merge blocks
  _parts.merge_part_blk(bg);
  
  // update block and partitions's time cost
  for(int i=0; i<bg.size(); i++)
    bg.cmpt_time(i);
  for(int i=0; i<_nPart; i++)
    _parts[i].cmpt_time(bg);

  // sort partitions
  _parts.build();

  return(0);
}


int Partitioner::adjust_greedy(BlockGraph& bg)
{
  int      pid, toPart;
  BlockCut cut;

  double tolerGrdy = 0.1;

  // set ideal work for all parts
  for(int i=0; i<_nPart; i++)
    _parts[i].set_ideal_work(_wkAvg);

#if verbose>=1
  cout << "--------------------------------\n" << "greedy adjustment" << endl;
#endif

  while(_parts.rank(0).work() < _wkAvg*(1.0-tolerGrdy)){
    // break out if most overload partition stays in range
    if(_parts.rank(_nPart-1).work() < _wkAvg*(1+_toler)) break;
    // set partition id that shift load
    toPart = _parts.rank(0).id;
    pid    = _parts.rank(_nPart-1).id;
    // find cut-off or block shifted
    _parts.find_part_shift(bg, pid, toPart, tolerGrdy, cut);
    if(cut.blkID == BLOCK_NULL){
#if verbose>=1
      cout << "Partitioner: Fail to adjust between " << pid << " " << toPart << endl;
#endif
      break;
    }
    else 
      _parts.shift_part(bg, toPart, cut);
  }

  // update block and partitions's time cost
  for(int i=0; i<bg.size(); i++)
    bg.cmpt_time(i);
  for(int i=0; i<_nPart; i++)
    _parts[i].cmpt_time(bg);

  // sort partitions
  _parts.build();

  return(0);
}


int Partitioner::exchange(BlockGraph& bg)
{
  BlockCut cut(DLARGE), cutTmp(DLARGE);
  int      pidFrom, pidTo;
  double   imblncMax = 0.5;

  // build up shift table
  _setup_exchange_tbl(bg);
  _tbl.view();

  // echange elements according to the table
  while(!_tbl.is_empty()){
    // pop the block with max reduction in communication time
    _tbl.pop(pidFrom, pidTo, cut);
    // move block to target partition
    _move_blk(bg, cut.blkID, pidFrom, pidTo);
    // remove shifts including the same block
    _tbl[pidFrom].del_element(cut);
    // update moved block's neighbor
    for(int iFc=0; iFc<6; iFc++){
      BlockFace *ptFc = bg[cut.blkID].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk != cut.blkID){
          // exclude block which may cause too much imbalance
          int    pid     = bg[iter->toBlk].partition_id();
          double imblnc  = bg[iter->toBlk].work() / _parts[pid].work();
          if(imblnc < imblncMax){
            // msg to source and target partition of cut.blkID
            double msgTo   = bg.msg_to_part(iter->toBlk, pidTo);
            double msgFrom = bg.msg_to_part(iter->toBlk, pidFrom);
            // msg to self partition, note that self can be pidFrom or pidTo,
            // the msg diff is 0, does not influence following adjust.
            double msgSelf = bg.msg_to_part(iter->toBlk, pid);
            // mark iter->toBlk's location in table row.
            int    idx     = INDEX_NULL; 
            // iter->toBlk, shift form pid to pidFrom, communication reduction is reduced
            // shift was in table
            idx = _tbl[pid].pos(pidFrom, iter->toBlk);
            if(msgFrom - msgSelf > DSMALL){
              if(idx == INDEX_NULL){
                cerr << "Partitioner ERROR: Unexpected null index." << endl;
              }
              // change the weight
              cutTmp.blkID   = iter->toBlk;
              cutTmp.msgIncr = msgSelf - msgFrom; // new weight
              _tbl[pid].chng_element(idx, pidFrom, cutTmp);
            }
            // shift has no benefit, should not stay in table
            else{
              // if find shift in table, delete it
              if(idx != INDEX_NULL)  _tbl[pid].del_element(idx);
            }
            // iter->toBlk, shift form pid to pidTo, communication reduction is gained
            // if shift should stay in table
            idx = _tbl[pid].pos(pidTo, iter->toBlk);
            if(msgTo - msgSelf > DSMALL){
              cutTmp.blkID   = iter->toBlk;
              cutTmp.msgIncr = msgSelf - msgTo; // new weight
              // if shift was in table, increase weight and move to correct position
              if(idx != INDEX_NULL)
                _tbl[pid].chng_element(idx, pidTo, cutTmp);
              // shift not in, add it
              else
                if(!_isMvds[iter->toBlk] && imblnc < imblncMax)
                  _tbl[pid].add(pidTo, cutTmp);
            }
            // shift was not it and should not be in, so do nothing.
          }
        }

        iter++;
      }
    }
  }

  _parts.merge_part_blk(bg);

  return(0);
}


int Partitioner::_setup_exchange_tbl(BlockGraph& bg)
{
  BlockCut cut(DLARGE);
  double   imblncMax = 0.5;

  for(int pidFrom=0; pidFrom<_nPart; pidFrom++){
    _tbl.append_row(pidFrom);
    for(unsigned int i=0; i<_parts[pidFrom].blkIDs.size(); i++){
      int    bid    = _parts[pidFrom].blkIDs[i];
      // check if moving this block cause too much imbalance
      double imblnc = bg[bid].work() / _parts[pidFrom].work();
      if(imblnc > imblncMax)  continue;
      // check if block should be added to table
      for(int pidTo=0; pidTo<_nPart; pidTo++){
        double msgSelf  = bg.msg_to_part(bid, pidFrom);
        double msgTrgt = bg.msg_to_part(bid, pidTo);
        if(msgTrgt - msgSelf > DSMALL){
          cut.msgIncr = msgSelf - msgTrgt;
          cut.blkID   = bid;
          _tbl.add(pidFrom, pidTo, cut);
        }
      }
    }
  }

  // mark all blocks as not moved
  _isMvds.clear();
  for(int i=0; i<bg.size(); i++){
    _isMvds.push_back(false);
  }

  return(0);
}


int Partitioner::_move_blk(BlockGraph& bg, int bid, int pid0, int pid1)
{
  // delete block bid from part pid0
  _parts[pid0].del_blk(bid);
  _parts.add_load(pid0, -bg[bid].work_load());
  _parts[pid0].del_shmem_map(bg, bid);
  
  // set new part id
  bg[bid].set_partition(pid1);

  // Add block bid to part pid1
  _parts[pid1].blkIDs.push_back(bid);
  _parts.add_load(pid1, bg[bid].work_load());
  _parts[pid1].add_shmem_map(bg, bid);

  // mark block as moved
  _isMvds[bid] = true;

#if verbose>=2
  cout << "Partitioner: Move block " << bid << " from " 
       << pid0 << " to " << pid1 << endl;
#endif

  return(0);
}
