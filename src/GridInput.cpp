#include "GridInput.h"

#include <cstring>


GridInput:: GridInput(const GridInput& rhs)
{
  _nBlk = rhs._nBlk;
  _blkBuf = new int [_nBlk];
  memcpy(_blkBuf, rhs._blkBuf, _nBlk*sizeof(int));
  _nMap = rhs._nMap;
  _mapBuf = new int [_nMap];
  memcpy(_mapBuf, rhs._mapBuf, _nMap*sizeof(int));
}


const GridInput& GridInput::operator=(const GridInput& rhs)
{
  if (this != &rhs) {
    _nBlk = rhs._nBlk;
    if (!_blkBuf) delete [] _blkBuf;
    _blkBuf = new int [_nBlk];
    memcpy(_blkBuf, rhs._blkBuf, _nBlk*sizeof(int));

    _nMap = rhs._nMap;
    if (!_mapBuf) delete [] _mapBuf;
    _mapBuf = new int [_nMap];
    memcpy(_mapBuf, rhs._mapBuf, _nMap*sizeof(int));
  }

  return *this;
}


GridInput::~GridInput()
{
  delete [] _blkBuf;
  delete [] _mapBuf;
}


int GridInput::bcast_block_info(const string& fname)
{
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if(rank == 0){
    ifstream input(fname, std::ios::binary);

    // get # blocks
    input.read(reinterpret_cast<char*>(&_nBlk), sizeof(int));

    // allocate buffer for block range
    _blkBuf = new int [3*_nBlk];

    // read block range
    for(int i=0; i<_nBlk; i++){
      input.read(reinterpret_cast<char*>(_blkBuf+3*i),   sizeof(int));
      input.read(reinterpret_cast<char*>(_blkBuf+3*i+1), sizeof(int));
      input.read(reinterpret_cast<char*>(_blkBuf+3*i+2), sizeof(int));
    }
  }

  // bcast # block
  MPI_Bcast(&_nBlk, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // non-root allocate buffer
  if(rank != 0)  _blkBuf = new int [3*_nBlk];
  
  // bcast # block ranges
  MPI_Bcast(_blkBuf, 3*_nBlk, MPI_INT, 0, MPI_COMM_WORLD);

  return(0);
}


int GridInput::bcast_map(const string& fname)
{
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // root count # map
  if(rank == 0){
    ifstream input(fname);
    string line;

    // get # map
    // skip the title line.
    getline(input, line);
    // count line of boundary condition
    _nMap = 0;
    while(getline(input, line))
      _nMap++;

    // reset to beginning of file, skip title line
    input.clear();
    input.seekg(0, ios::beg);
    getline(input, line);

    // allocate buffer
    _mapBuf = new int [14*_nMap];

    // read map line
    int iMap = 0;
    while(getline(input, line)){
      // read bc type
      string bcStr;
      istringstream strIn(line);
      getline(strIn, bcStr, ' ');

      // check bc type
      if(bcStr.compare("FarField")    == 0)  _mapBuf[14*iMap] = BC_FARFIELD;
      if(bcStr.compare("ViscousWall") == 0)  _mapBuf[14*iMap] = BC_VISCOUSWALL;
      if(bcStr.compare("EulerWall")   == 0)  _mapBuf[14*iMap] = BC_EULERWALL;
      if(bcStr.compare("Symmetry")    == 0)  _mapBuf[14*iMap] = BC_SYMMETRY;
      if(bcStr.compare("Block2Block") == 0)  _mapBuf[14*iMap] = BC_BLOCK2BLOCK;

      // read block, face ID
      strIn >> _mapBuf[14*iMap+1] >> _mapBuf[14*iMap+2];
      _mapBuf[14*iMap+1]--;
      _mapBuf[14*iMap+2]--;

      // read range and flip based on bc type
      if(bcStr.compare("Block2Block") == 0){
        for(int i=3; i<=12; i++){
          strIn >> _mapBuf[14*iMap+i];
          _mapBuf[14*iMap+i]--;
        }
        string flipStr;
        strIn >> ws >> flipStr;
        _mapBuf[14*iMap+13] = flipStr.compare("yes") == 0 ? 1 : 0;
      }
      else{
        strIn >> ws;
        if(!strIn.eof()){
          for(int i=3; i<=7; i++){
            strIn >> _mapBuf[14*iMap+i];
            _mapBuf[14*iMap+i]--;
          }
        }
        else{
          for(int i=3; i<=7; i++)
            _mapBuf[14*iMap+i] = INDEX_NULL;
        }
        for(int i=8; i<=13; i++)
          _mapBuf[14*iMap+i] = INDEX_NULL;
      }

      iMap++;
    }
  }

  // bcast # map
  MPI_Bcast(&_nMap, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // non-root allocate buffer
  if(rank != 0)  _mapBuf = new int [14*_nMap];

  // bcast map buffer
  MPI_Bcast(_mapBuf, 14*_nMap, MPI_INT, 0, MPI_COMM_WORLD);

  return(0);
}
