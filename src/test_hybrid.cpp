#include <stdlib.h>
#include <iostream>
#include "BlockGraph.h"
#include "BlockPart.h"
#include "PartMethod.h"
#include "Partitioner.h"
#include "HybridPartitioner.h"

using namespace std;

int main(int argc, char* argv[])
{
  if(argc < 3 && argc > 11){
    cerr << "Wrong number of command arguments." << endl;
    cout << "The correct usage is ./[executable] [#part0] [#part1] "
         << "(-method [meth0] [meth1]) (-adjust [adjust]) " 
         << "(-file [mesh] [mapfile])" << endl;
    exit(-1);
  }

  int         nProc = atoi(argv[2]);
  BlockGraph  geo, grid;
  vector<int> bids;
  string      meshName("mesh.x"), mapName("mapfile");
  string      mthdName0("graphgrow"), mthdName1("greedycut"), adj("null");
  Method_t    mthd0, mthd1;

  for(int i=2; i<argc; i++){
    if(strcmp(argv[i], "-method") == 0){
      mthdName0 = argv[i+1];
      mthdName1 = argv[i+2];
    }
    if(strcmp(argv[i], "-adjust") == 0){
      adj = argv[i+1];
    }
    if(strcmp(argv[i], "-file")   == 0){
      meshName = argv[i+1];
      mapName  = argv[i+2];
    }
  }

  geo.setup_msg_load(3.8469e-5, 1.0/2.182e-9, 8);
  geo.set_cell_time(5.0e-8);
  geo.fread_mesh_plot3d(meshName, false);
  geo.fread_map(mapName);

  // create and setup partition
  HybridPartitioner pttnr(nProc);
  pttnr.set_tolerance(0.02);
  pttnr.set_1st_partition_size(atoi(argv[1]));

  // method
  if(mthdName0.compare("graphgrow")  == 0)  mthd0 = GraphGrow; 
  if(mthdName0.compare("multilevel") == 0)  mthd0 = MultiLevel; 
  if(mthdName1.compare("greedycut")  == 0)  mthd1 = GreedyCut; 
  if(mthdName1.compare("greedyzone") == 0)  mthd1 = GreedyZone; 
  pttnr.setup_partition_method(mthd0, mthd1);
  
  // partition
  pttnr.decompose(geo, grid);

  // adjust
  // adjust
  if(adj.compare("null") != 0){
    if(adj.compare("all")    == 0)  pttnr.adjust_all(grid);
    if(adj.compare("greedy") == 0)  pttnr.adjust_greedy(grid);
  }

  // evaluate partition properties
  pttnr.eval_part_prop(grid);
  pttnr.view_part_prop();

  // write grid
  // grid.copy_coord(geo);
  // grid.fwrite_mesh_tecplot("decomp_hybrid.plt");

  return(0);
}
