#include <stdlib.h>
#include <iostream>
#include "BlockGraph.h"
#include "BlockPart.h"
#include "PartMethod.h"
#include "Partitioner.h"
#include <mpi.h>

using namespace std;

int main(int argc, char* argv[])
{
  if(argc < 2 && argc > 9){
    cerr << "Wrong number of command arguments." << endl;
    cout << "The correct usage is ./[executable] [#proc] "
         << "(-method [method]) (-adjust [adjust]) " 
         << "(-file [mesh] [mapfile])" << endl;
    exit(-1);
  }

  int         nProc = atoi(argv[1]);
  BlockGraph  geo, geo_return, grid, geo_test, grid_test;
  vector<int> bids;
  string      meshName("mesh.x"), mapName("mapfile");
  string      mthdName("gc"), adjName("null");

  for(int i=2; i<argc; i++){
    if(strcmp(argv[i], "-method") == 0)  mthdName = argv[i+1];
    if(strcmp(argv[i], "-adjust") == 0)   adjName = argv[i+1];
    if(strcmp(argv[i], "-file")   == 0)  meshName = argv[i+1];
    if(strcmp(argv[i], "-file")   == 0)   mapName = argv[i+2];
  }

  geo.setup_msg_load(3.8469e-5, 1.0/2.182e-9, 8);
  geo.set_cell_time(5.0e-8);
  geo.fread_mesh_plot3d(meshName, true);
  geo.fread_map(mapName);
  // geo.fwrite_cgns("uneven_channel", true);
  // geo.fwrite_map("uneven_channel");
  // geo.fread_cgns("backstep_modified", "Partition", true);
  geo.block_coalesce(geo_test);
  geo_test.fwrite_cgns("hahaha", false);


  // create and setup partition
  Partitioner pttnr(nProc);
  pttnr.set_tolerance(0.05);

  // set partition method
  if(mthdName.compare("gc") == 0)  pttnr.setup_partition_method(GreedyCut);
  if(mthdName.compare("gz") == 0)  pttnr.setup_partition_method(GreedyZone);
  if(mthdName.compare("gg") == 0)  pttnr.setup_partition_method(GraphGrow);
  if(mthdName.compare("ml") == 0)  pttnr.setup_partition_method(MultiLevel);
  if(mthdName.compare("pg") == 0)  pttnr.setup_partition_method(PureGreedy);
  if(mthdName.compare("pc") == 0)  pttnr.setup_partition_method(PureGreedyCut);

  // partition
  pttnr.decompose(geo, grid);

  // adjust
  if(adjName.compare("null") != 0){
    if(adjName.compare("all")    == 0)  pttnr.adjust_all(grid);
    if(adjName.compare("greedy") == 0)  pttnr.adjust_greedy(grid);
  }

  grid.debug_check();

  // evaluate partition properties
  pttnr.eval_part_prop(grid);
  pttnr.view_part_prop();
  cout << "#blocks " << grid.size() << endl;

  // write grid
  grid.copy_coord(geo);
  grid.fwrite_cgns("uneven_cut", true);
  // grid.fwrite_map("uneven_cut");

  // test cgns I/O after partitioning
  // grid_test.setup_msg_load(3.8469e-5, 1.0/2.182e-9, 8);
  // grid_test.set_cell_time(5.0e-8);
  // grid_test.fread_cgns("coord_part_before", "Base", false);
  // grid_test.fwrite_cgns("coord_part_after", false);
  // grid_test.fwrite_map("map_part_after");
  // grid_test.fwrite_block_info("block_info_part_after");
  // grid.fwrite_mesh_tecplot("decomp.plt");

  return(0);
}
