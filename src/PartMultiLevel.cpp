#include "PartMultiLevel.h"


int PartMultiLevel::decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid)
{
  // save graph, # partitions
  grid   = geo;
  _nPart = nPart;
#if verbose>=1
  cout << "MultiLevel: init grid has " << geo.size() << " blocks." << endl;
#endif

  // average work load
  double wkTot = 0.0;
  for(unsigned int i=0; i<bids.size(); i++)
    wkTot += (double)grid[bids[i]].work_load();
  _wkAvg = wkTot / _nPart;

  // cut large blocks into unit block
  //grid.cut_to_elements();
  //cut_large_block(grid, bids);
  _cut_blks_to_units(grid);
#if verbose>=1
  cout << "MultiLevel: cut grid into " << grid.size() << " blocks." << endl;
#endif

  // setup metis's arguments
  _setup_metis_args(grid);
  //view_metis_args();

  int objval;
  METIS_PartGraphKway(&_nVtx, &_nCon, _xadjs, _adjncys, _vwgts, NULL, _adjwgts, \
      &_nPart, _tpwgts, _ubvec, _options, &objval, _partIDs);
  //METIS_PartGraphRecursive(&_nVtx, &_nCon, _xadjs, _adjncys, _vwgts, NULL, _adjwgts, \
  //&_nPart, _tpwgts, _ubvec, _options, &objval, _partIDs);

  // set partition id
  for(int i=0; i<grid.size(); i++){
    grid[i].set_partition((int)_partIDs[i]);
  }

  _clear_metis_args();

  return(0);
}


int PartMultiLevel::view_metis_args()
{
  cout << "# vertex: " << _nVtx  << endl;
  cout << "# edge:   " << _nEdge << endl;
  // starting index
  cout << "---- xadj ----" << endl;
  for(int i=0; i<_nVtx+1; i++){
    cout << _xadjs[i] << endl;
  }

  // edges
  cout << "---- adjncy, adjwgt ----" << endl;
  for(int i=0; i<_nEdge; i++){
    cout << _adjncys[i] << " " << _adjwgts[i] << endl;
  }

  // vertex weight
  cout << "---- vwgt ----" << endl;
  for(int i=0; i<_nVtx; i++){
    cout << _vwgts[i] << endl;
  }

  return(0);
}


int PartMultiLevel::cut_large_block(BlockGraph& bg, vector<int>& bids)
{
  for (int i=0; i<bg.size(); ++i) {
    // if the block is larger than average
    if (bg[i].work() > _wkAvg * (1.0+_toler)) {
      int npMain  = (int)floor(bg[i].work() / _wkAvg); // # parts of main part
      int bidMain = i; // block id
      // if residual part exists
      if (bg[i].work() > npMain*_wkAvg*(1.0+_toler)) {
        // find the cut of residual part
        double wkCut    = bg[i].work() - npMain * _wkAvg;
        double tolerCut = min(npMain*_wkAvg*_toler / wkCut, 0.5);
        BlockCut cut;
        bg.find_min_blkcut(i, wkCut, tolerCut, cut);
        // if cut is valid, cut off the resiual part
        if (cut.blkID != BLOCK_NULL) {
          bg.cut_block(cut);
          bidMain = (cut.isKept) ? i : bg.size()-1;
        }
      }
      // recursively bisect the main part
      bg.reb(bidMain, npMain, _toler);
        //vector<int> preBids;
        //bg.factorize_block(bidMain, preBids, npMain, _toler);
    }
  }

  return 0;
}


int PartMultiLevel::_setup_metis_args(BlockGraph& bg)
{
  // # vertices, # weight per vectex
  _nVtx = bg.size();
  _nCon = 1;

  // # edges, exclude self mapping, which is shared mem in init
  _nEdge = 0;
  for(int i=0; i<bg.size(); i++){
    for(int j=0; j<6; j++){
      BlockFace* ptFc = bg[i].face_pt(j);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk != i){
          _nEdge++;
        }
        iter++;
      }
    }
  }
  
  // allocate arguments for metis
  _xadjs    = new idx_t[_nVtx+1];
  _xadjs[0] = 0;
  _vwgts    = new idx_t[_nVtx * _nCon];
  _adjncys  = new idx_t[_nEdge];
  _adjwgts  = new idx_t[_nEdge];
  _partIDs  = new idx_t[_nVtx];
  _tpwgts   = new real_t[_nPart];
  _ubvec    = new real_t[_nCon];

  fill(_adjncys, _adjncys+_nEdge, (idx_t)(bg.size()+1));
  fill(_tpwgts,  _tpwgts +_nPart, (real_t)(1.0/_nPart));
  fill(_ubvec,   _ubvec  +_nCon,  (real_t)1.1);

  // set options
  METIS_SetDefaultOptions(_options);
  _options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;

  // set up graph info
  for(int i=0; i<bg.size(); i++){
    idx_t nB2b   = 0;
    for(int j=0; j<6; j++){
      BlockFace* ptFc = bg[i].face_pt(j);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk != i){
          // try to find position of toBlk in adjncys
          idx_t pos = _xadjs[i];
          for (pos=_xadjs[i]; pos<_xadjs[i]+nB2b; ++pos)
            if (_adjncys[pos] == iter->toBlk) break;
          // if this target has not been added
          if (pos == _xadjs[i] + nB2b) {
            _adjncys[_xadjs[i]+nB2b] = iter->toBlk;
            _adjwgts[_xadjs[i]+nB2b] = max(iter->rngs[3] - iter->rngs[0], 1) \
                                     * max(iter->rngs[4] - iter->rngs[1], 1) \
                                     * max(iter->rngs[5] - iter->rngs[2], 1);
            nB2b++;
          }
          else {
            _adjwgts[pos] += max(iter->rngs[3] - iter->rngs[0], 1) \
                           * max(iter->rngs[4] - iter->rngs[1], 1) \
                           * max(iter->rngs[5] - iter->rngs[2], 1);
          }
        }
        iter++;
      }
    }
    // vertex weight
    _vwgts[i] = bg[i].work_load();
    // set next vertex's edge start index
    _xadjs[i+1] = _xadjs[i] + nB2b;
  }


  return(0);
}


int PartMultiLevel::_clear_metis_args()
{
  delete [] _xadjs;
  delete [] _vwgts;
  delete [] _adjwgts;
  delete [] _adjncys;
  delete [] _partIDs;
  delete [] _tpwgts;
  delete [] _ubvec;

  return(0);
}


int PartMultiLevel::_cut_blks_to_units(BlockGraph& bg)
{
  // unit work load
  double wkUnit = _wkAvg * _unitRatio;

  // save graph size
  int size0 = bg.size();

  // cut block to units
  for(int i=0; i<size0; i++){
    int nSubBlk = (int)round(bg[i].work_load() / wkUnit);
    //if(nSubBlk > 1)  bg.cut_block(i, nSubBlk);
    if(nSubBlk > 1)  bg.reb(i, nSubBlk, _toler);
  }

  return(0);
}
